import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BaseBrickComponent } from './components/base-brick/base-brick.component';
import { BetonBrickComponent } from './components/beton-brick/beton-brick.component';
import { BlockBrickComponent } from './components/block-brick/block-brick.component';
import { MapX1Component } from './components/x1/map-x1/map-x1.component';
import { MapX2Component } from './components/x1/map-x2/map-x2.component';
import { MapX4Component } from './components/x1/map-x4/map-x4.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseBrickComponent,
    BetonBrickComponent,
    BlockBrickComponent,
    MapX1Component,
    MapX2Component,
    MapX4Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
