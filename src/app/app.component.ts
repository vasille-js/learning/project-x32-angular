import { Component } from '@angular/core';
import {BetonBlock, BlockPack, BrickBlock} from './types/block';

const s = 40;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project-x32-angular';
  blocks: BlockPack;

  constructor() {
    this.blocks = new BlockPack(0, 0, 26 * s, 26 * s);
    this.createLevel([
      [2, 1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [1, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 2, 2, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 2, 2, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],

      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 0, 0, 0, 1, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 1, 1, 1, 0, 0,  0, 0],
      [2, 2, 0, 0, 1, 1, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 1, 1, 1, 0, 0,  2, 2],
      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],

      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 1, 1, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 1, 1, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 1, 1, 1, 1, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],

      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 1, 0, 0, 1, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 1, 0, 0, 1, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0]
    ]);
  }

  createLevel(data: number[][]): void {
    for (const i in data) {
      if (data[i]) {
        for (const j in data[i]) {
          if (data[i]) {
            const v = data[i][j];

            if (v !== 0) {
              if (v === 1) {
                // @ts-ignore
                this.blocks.els.push(new BrickBlock(j * s, i * s));
              }
              else if (v === 2) {
                // @ts-ignore
                this.blocks.els.push(new BetonBlock(j * s, i * s));
              }
            }
          }
        }
      }
    }

    const fastFind: { [p: number]: { [p: number]: BlockPack } } = { 0: {}, 1: {}, 2: {}, 3: {} };
    let ls = 8 * s;

    for (let x = 0; x < 4; x++) {
      for (let y = 0; y < 4; y++) {
        const blockPack = new BlockPack( x * ls, y * ls, ls, ls);
        this.blocks.sub.push(blockPack);
        fastFind[x][y] = blockPack;
      }
    }

    for (const block of this.blocks.els) {

      fastFind
        [Math.floor(block.x / ls)]
        [Math.floor(block.y / ls)]
        .els.push(block);
    }

    for (const sub of this.blocks.sub) {
      if (sub.els.length === 0) {
        this.blocks.sub.splice(this.blocks.sub.indexOf(sub), 1);
      }
    }

    ls = 2 * s;

    for (const pack of this.blocks.sub) {

      for (let x = 0; x < 4; x++) {
        for (let y = 0; y < 4; y++) {
          const blockPack = new BlockPack(pack.x + x * ls, pack.y + y * ls, ls, ls);
          pack.sub.push(blockPack);
          fastFind[x][y] = blockPack;
        }
      }

      for (const block of pack.els) {

        fastFind
          [Math.floor((block.x - pack.x) / ls)]
          [Math.floor((block.y - pack.y) / ls)]
          .els.push(block);
      }

      for (const sub of pack.sub) {
        if (sub.els.length === 0) {
          pack.sub.splice(pack.sub.indexOf(sub), 1);
        }
      }
    }
  }

}
