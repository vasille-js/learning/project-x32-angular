import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseBrickComponent } from './base-brick.component';

describe('BaseBrickComponent', () => {
  let component: BaseBrickComponent;
  let fixture: ComponentFixture<BaseBrickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseBrickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseBrickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
