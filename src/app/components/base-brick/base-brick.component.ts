import {Component, Input, OnInit} from '@angular/core';
import { observable, computed } from 'mobx-angular';

@Component({
  selector: 'app-base-brick',
  templateUrl: './base-brick.component.html',
  styleUrls: ['./base-brick.component.css']
})
export class BaseBrickComponent implements OnInit {

  @Input() @observable x: number;
  @Input() @observable y: number;
  @Input() @observable tx: number;
  @Input() @observable ty: number;
  @Input() @observable l1x: number;
  @Input() @observable l2x: number;
  @Input() @observable l1y: number;
  @Input() @observable l2y: number;
  @Input() @observable step: number;
  @Input() @observable scale: number;
  @Input() @observable className: string;
  @Input() @observable cw: number;
  @Input() @observable ch: number;

  @computed get cx(): number {
    return this.step < 2 || this.step > 4
      ? this.x * this.step
      : this.step === 2
        ? 2 * (this.x - this.l1x)
        : 4 * (this.x - this.l2x);
  }
  @computed get cy(): number {
    return this.step < 2 || this.step > 4
      ? this.y * this.step
      : this.step === 2
        ? 2 * (this.y - this.l1y)
        : 4 * (this.y - this.l2y);
  }
  @computed get lscale(): number {
    return this.scale / this.step;
  }
  @computed get lWidth(): number {
    return 40 * this.step;
  }
  @computed get lHeight(): number {
    return 40 * this.step;
  }
  @computed get left(): number {
    return this.x * this.scale + this.tx;
  }
  @computed get right(): number {
    return this.left + this.lWidth * this.scale;
  }
  @computed get top(): number {
    return this.y * this.scale + this.ty;
  }
  @computed get bottom(): number {
    return this.top + this.lHeight * this.scale;
  }
  @computed get hVisible(): boolean {
    return this.left < this.cw && this.right > 0;
  }
  @computed get vVisible(): boolean {
    return this.top < this.ch && this.bottom > 0;
  }
  @computed get visible(): boolean {
    return (this.hVisible && this.vVisible && this.step > 4) || this.step <= 4;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
