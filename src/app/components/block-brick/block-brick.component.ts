import {Component, Input, OnInit} from '@angular/core';
import {observable} from 'mobx-angular';

@Component({
  selector: 'app-block-brick',
  templateUrl: './block-brick.component.html',
  styleUrls: ['./block-brick.component.css']
})
export class BlockBrickComponent implements OnInit {

  @Input() @observable x: number;
  @Input() @observable y: number;
  @Input() @observable tx: number;
  @Input() @observable ty: number;
  @Input() @observable l1x: number;
  @Input() @observable l2x: number;
  @Input() @observable l1y: number;
  @Input() @observable l2y: number;
  @Input() @observable step: number;
  @Input() @observable scale: number;
  @Input() @observable className: string;
  @Input() @observable cw: number;
  @Input() @observable ch: number;

  constructor() { }

  ngOnInit(): void {
  }

}
