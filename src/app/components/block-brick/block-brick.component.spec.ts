import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockBrickComponent } from './block-brick.component';

describe('BlockBrickComponent', () => {
  let component: BlockBrickComponent;
  let fixture: ComponentFixture<BlockBrickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlockBrickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockBrickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
