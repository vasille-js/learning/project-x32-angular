import {Component, Input, OnInit} from '@angular/core';
import {computed, observable} from 'mobx-angular';
import {Block, BlockPack} from '../../../types/block';

@Component({
  selector: 'app-map-x1',
  templateUrl: './map-x1.component.html',
  styleUrls: ['./map-x1.component.css']
})
export class MapX1Component implements OnInit {

  @Input() @observable x: number;
  @Input() @observable y: number;
  @Input() @observable width: number;
  @Input() @observable height: number;
  @Input() @observable els: Array<Block>;
  @Input() @observable sub: Array<BlockPack>;

  @observable tx = 0;
  @observable ty = 0;
  @observable scale = 1;
  @observable cw = 0;
  @observable ch = 0;
  @observable rtx = 0;
  @observable rty = 0;
  @observable rscale = 0;
  @observable btx = 0;
  @observable bty = 0;
  @observable bscale = 0;
  @observable beginTime = 0;
  @observable endTime = 0;
  @observable isAnim = false;

  @computed get stepScale(): number {
    if (this.scale <= 1) {
      return 1;
    }
    if (this.scale <= 2) {
      return 2;
    }
    if (this.scale <= 4) {
      return 4;
    }
    if (this.scale <= 8) {
      return 8;
    }
    if (this.scale <= 16) {
      return 16;
    }
    if (this.scale <= 32) {
      return 32;
    }
    return this.scale;
  }

  constructor() { }

  ngOnInit(): void {
    document.onmousedown = press => {
      const tx = this.tx;
      const ty = this.ty;

      press.preventDefault();

      document.onmousemove = move => {
        this.tx = this.rtx = tx + (move.screenX - press.screenX);
        this.ty = this.rty = ty + (move.screenY - press.screenY);

        move.preventDefault();
      };

      document.onmouseup = up => {
        document.onmousemove = null;
        document.onmouseup = null;
        up.preventDefault();
      };
    };

    document.onwheel = wheel => {
      const step = this.stepScale;
      const cscale = this.rscale || this.scale;
      const ctx = this.rtx || this.tx;
      const cty = this.rty || this.ty;
      let nscale;

      if (wheel.deltaY < 0) {
        const v = cscale + 0.1 * step;

        if (v < 32) {
          nscale = v;
        }
        else {
          nscale = 32;
        }
      }
      else {
        const v = cscale - 0.1 * step;

        if (v > 0.5) {
          nscale = v;
        }
        else {
          nscale = 0.5;
        }
      }

      this.animTo(
        ctx - (nscale - cscale) * (wheel.clientX - ctx) / cscale,
        cty - (nscale - cscale) * (wheel.clientY - cty) / cscale,
        nscale
      );
    };

    window.onkeypress = () => {
      // @ts-ignore
      document.onwheel({deltaY: -1, clientX: 0, clientY: 0});
    };

    window.onresize = () => {
      this.cw = window.innerWidth;
      this.ch = window.innerHeight;
    };

    window.onresize(null);
  }
  animTo(x: number, y: number, scale: number): void {
    this.rtx = x;
    this.rty = y;
    this.rscale = scale;
    this.btx = this.tx;
    this.bty = this.ty;
    this.bscale = this.scale;

    this.beginTime = new Date().getTime();
    this.endTime = this.beginTime + 300;

    if (!this.isAnim) {
      this.isAnim = true;

      const update = () => {
        const now = new Date().getTime();

        if (now > this.endTime) {
          this.isAnim = false;
          this.tx = this.rtx;
          this.ty = this.rty;
          this.scale = this.rscale;
        }
        else {
          const progress = (now - this.beginTime) / 300;
          const step = Math.cos(Math.PI * progress) - 1;

          this.tx = -(this.rtx - this.btx) / 2 * step + this.btx;
          this.ty = -(this.rty - this.bty) / 2 * step + this.bty;
          this.scale = -(this.rscale - this.bscale) / 2 * step + this.bscale;

          window.requestAnimationFrame(update);
        }
      };

      window.requestAnimationFrame(update);
    }
  }
}
