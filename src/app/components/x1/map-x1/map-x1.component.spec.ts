import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapX1Component } from './map-x1.component';

describe('MapX1Component', () => {
  let component: MapX1Component;
  let fixture: ComponentFixture<MapX1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapX1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapX1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
