import {Component, Input, OnInit} from '@angular/core';
import {computed, observable} from 'mobx-angular';
import {Block, BlockPack} from '../../../types/block';

@Component({
  selector: 'app-map-x2',
  templateUrl: './map-x2.component.html',
  styleUrls: ['./map-x2.component.css']
})
export class MapX2Component implements OnInit {

  @Input() @observable x: number;
  @Input() @observable y: number;
  @Input() @observable width: number;
  @Input() @observable height: number;
  @Input() @observable els: Array<Block>;
  @Input() @observable sub: Array<BlockPack>;
  @Input() @observable tx: number;
  @Input() @observable ty: number;
  @Input() @observable scale: number;
  @Input() @observable cw: number;
  @Input() @observable ch: number;
  @Input() @observable stepScale: number;

  @computed get lWidth(): number {
    return this.width * 2;
  }
  @computed get lHeight(): number {
    return this.height * 2;
  }
  @computed get lScale(): number {
    return this.scale / 2;
  }
  @computed get left(): number {
    return this.x * this.scale + this.tx;
  }
  @computed get right(): number {
    return this.left + this.lWidth * this.scale;
  }
  @computed get top(): number {
    return this.y * this.scale + this.ty;
  }
  @computed get bottom(): number {
    return this.top + this.lHeight * this.scale;
  }
  @computed get hVisible(): boolean {
    return this.left < this.cw && this.right > 0;
  }
  @computed get vVisible(): boolean {
    return this.top < this.ch && this.bottom > 0;
  }
  @computed get visible(): boolean {
    return (this.hVisible && this.vVisible && this.stepScale === 2) || this.stepScale !== 2;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
