import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapX2Component } from './map-x2.component';

describe('MapX2Component', () => {
  let component: MapX2Component;
  let fixture: ComponentFixture<MapX2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapX2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapX2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
