import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapX4Component } from './map-x4.component';

describe('MapX4Component', () => {
  let component: MapX4Component;
  let fixture: ComponentFixture<MapX4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapX4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapX4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
