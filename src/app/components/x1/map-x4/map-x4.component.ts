import {Component, Input, OnInit} from '@angular/core';
import {computed, observable} from 'mobx-angular';
import {Block, BlockPack} from '../../../types/block';

@Component({
  selector: 'app-map-x4',
  templateUrl: './map-x4.component.html',
  styleUrls: ['./map-x4.component.css']
})
export class MapX4Component implements OnInit {

  @Input() @observable x: number;
  @Input() @observable y: number;
  @Input() @observable width: number;
  @Input() @observable height: number;
  @Input() @observable els: Array<Block>;
  @Input() @observable sub: Array<BlockPack>;
  @Input() @observable tx: number;
  @Input() @observable ty: number;
  @Input() @observable scale: number;
  @Input() @observable cw: number;
  @Input() @observable ch: number;
  @Input() @observable stepScale: number;
  @Input() @observable l1x: number;
  @Input() @observable l1y: number;

  @computed get lWidth(): number {
    return this.width * 4;
  }
  @computed get lHeight(): number {
    return this.height * 4;
  }
  @computed get lScale(): number {
    return this.scale / 4;
  }
  @computed get left(): number {
    return this.x * this.scale + this.tx;
  }
  @computed get right(): number {
    return this.left + this.lWidth * this.scale;
  }
  @computed get top(): number {
    return this.y * this.scale + this.ty;
  }
  @computed get bottom(): number {
    return this.top + this.lHeight * this.scale;
  }
  @computed get hVisible(): boolean {
    return this.left < this.cw && this.right > 0;
  }
  @computed get vVisible(): boolean {
    return this.top < this.ch && this.bottom > 0;
  }
  @computed get visible(): boolean {
    return (this.hVisible && this.vVisible && this.stepScale === 4) || this.stepScale !== 4;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
