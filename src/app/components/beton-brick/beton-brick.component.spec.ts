import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetonBrickComponent } from './beton-brick.component';

describe('BetonBrickComponent', () => {
  let component: BetonBrickComponent;
  let fixture: ComponentFixture<BetonBrickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BetonBrickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetonBrickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
