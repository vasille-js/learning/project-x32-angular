
export class Block {
  x: number;
  y: number;
  type: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}

export class BrickBlock extends Block {
  constructor(x: number, y: number) {
    super(x, y);
    this.type = 1;
  }
}

export class BetonBlock extends Block {
  constructor(x: number, y: number) {
    super(x, y);
    this.type = 2;
  }
}


export class BlockPack {
  x: number;
  y: number;
  width: number;
  height: number;
  els: Array<Block>;
  sub: Array<BlockPack>;

  constructor(x: number, y: number, width: number, height: number) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.els = new Array<Block>();
    this.sub = new Array<BlockPack>();
  }
}
